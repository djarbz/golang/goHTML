package gohtml

import (
	"bytes"
	"fmt"
	"io"
	"os"
	"sync"
)

// viewTemplate is used to create a Layout or Contents template
type viewTemplate struct {
	parent     *templates
	name       string
	template   string
	components []string
	sync.RWMutex
}

// addTemplate will attach a Layout or Content to this template
func (v *viewTemplate) addTemplate(name string, templ io.Reader) error {
	buf := new(bytes.Buffer)
	if _, err := buf.ReadFrom(templ); err != nil {
		return err
	}

	v.Lock()
	defer v.Unlock()

	v.template = buf.String()
	v.name = name

	return nil
}

// AddComponent will attach a component to this template
func (v *viewTemplate) AddComponent(name string, templ io.Reader) error {
	name = fixName(name)

	// Check if component already exists in the cache
	if err := v.parent.AddComponent(name, templ); err != nil && err != ErrTemplateExists {
		return err
	}

	// Check if component is already associated to this view
	v.Lock()
	defer v.Unlock()
	if !stringInSlice(name, v.components) {
		v.components = append(v.components, name)
	}

	return nil
}

// AddComponentFromFile will attach a component to this template
func (v *viewTemplate) AddComponentFromFile(filePath string) error {
	reader, err := os.Open(filePath)
	if err != nil {
		return err
	}
	defer func(file string) {
		if err = reader.Close(); err != nil {
			fmt.Printf("Could not close file: %s\nError: %s", file, err)
		}
	}(filePath)

	return v.AddComponent(filePath, reader)
}
