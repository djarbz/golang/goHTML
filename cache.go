package gohtml

import (
	"bytes"
	"errors"
	"fmt"
	"html/template"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"strings"
	"sync"
)

func init() {
	Cache.Init()
}

type templateType int

type templateCache map[string]*viewTemplate

// layout designates the template type as a layout Template
const layout templateType = 1

// page designates the template type as a page Template
const page templateType = 2

// component designates the template type as a component Template
const component templateType = 3

// ErrInvalidTemplateType is returned when an invalid template type is provided
var ErrInvalidTemplateType = errors.New("invalid template type")

// ErrFuncExists is returned when trying to add a function name that is already in the func map
var ErrFuncExists = errors.New("function name already exists")

// ErrTemplateExists is returned when trying to add a template name that is already in the template cache
var ErrTemplateExists = errors.New("template name already exists")

// ErrTemplateNotFound is returned when attempting to compile with a template that does not exist in the cache
var ErrTemplateNotFound = errors.New("template was not found")

// Cache is the package global template cache
var Cache templates

type templates struct {
	layouts      templateCache
	components   map[string]string
	pages        templateCache
	funcs        template.FuncMap
	Debug        bool
	OutputToDisk bool
	OutputPath   string
	once         sync.Once
	buffer
	sync.RWMutex
}

// Init will setup the maps & buffer pool
func (t *templates) Init() {
	t.Lock()
	t.once.Do(func() {
		t.layouts = make(templateCache, 1)
		t.components = make(map[string]string, 1)
		t.pages = make(templateCache, 1)
		t.funcs = make(template.FuncMap, 1)
		t.SetBufPoolSize(15360)
		if t.OutputPath == "" {
			t.OutputPath = "viewTemplates"
		}
	})
	t.Unlock()
}

// viewTemplateFromFile will open the file to be read as a template
func (t *templates) viewTemplateFromFile(tType templateType, filePath string, components ...string) (*viewTemplate, error) {
	reader, err := os.Open(filePath)
	if err != nil {
		return nil, err
	}
	defer func(file string) {
		if err = reader.Close(); err != nil {
			fmt.Printf("Could not close file: %s\nError: %s", file, err)
		}
	}(filePath)

	switch tType {
	case layout:
		return t.viewTemplateFromReader(layout, filePath, reader, components...)
	case page:
		return t.viewTemplateFromReader(page, filePath, reader, components...)
	case component:
		return t.viewTemplateFromReader(component, filePath, reader, components...)
	default:
		return nil, ErrInvalidTemplateType
	}
}

// viewTemplateFromReader will import a template from a reader
func (t *templates) viewTemplateFromReader(tType templateType, name string, templ io.Reader, components ...string) (*viewTemplate, error) {
	name = fixName(name)

	var cache templateCache

	switch tType {
	case layout:
		cache = t.layouts
	case page:
		cache = t.pages
	case component:
		return nil, ErrInvalidTemplateType
	default:
		return nil, ErrInvalidTemplateType
	}

	t.RLock()
	_, ok := cache[name]
	t.RUnlock()

	if ok {
		return nil, ErrTemplateExists
	}

	view := new(viewTemplate)
	view.parent = t
	if err := view.addTemplate(name, templ); err != nil {
		return nil, err
	}

	t.Lock()
	cache[name] = view
	t.Unlock()

	for _, component := range components {
		if err := view.AddComponentFromFile(component); err != nil {
			return nil, err
		}
	}
	return view, nil
}

// NewLayout returns a view template that can be populated with view template data
func (t *templates) NewLayout(name string, templ io.Reader, components ...string) (*viewTemplate, error) {
	return t.viewTemplateFromReader(layout, name, templ, components...)
}

// NewLayoutFromFile will read a file to a view template
func (t *templates) NewLayoutFromFile(filePath string, components ...string) (*viewTemplate, error) {
	return t.viewTemplateFromFile(layout, filePath, components...)
}

// NewPage returns a view template that can be populated with view template data
func (t *templates) NewPage(name string, templ io.Reader, components ...string) (*viewTemplate, error) {
	return t.viewTemplateFromReader(page, name, templ, components...)
}

// NewPageFromFile will read a file to a view template
func (t *templates) NewPageFromFile(filePath string, components ...string) (*viewTemplate, error) {
	return t.viewTemplateFromFile(page, filePath, components...)
}

// AddFunc will append given function to the view FuncMap
func (t *templates) AddFunc(name string, function interface{}) error {
	if t.Debug {
		fmt.Printf("Importing Function: %s\n", name)
	}
	t.RLock()
	_, ok := t.funcs[name]
	t.RUnlock()

	if ok {
		return ErrFuncExists
	}

	t.Lock()
	defer t.Unlock()
	t.funcs[name] = function
	return nil
}

// AddFuncs will append given functions to the view FuncMap
func (t *templates) AddFuncs(functions template.FuncMap) error {
	for k, v := range functions {
		if err := t.AddFunc(k, v); err != nil {
			return err
		}
	}
	return nil
}

// AddComponent will read in a new layout component
func (t *templates) AddComponent(name string, templ io.Reader) error {
	name = fixName(name)

	if t.Debug {
		fmt.Printf("Loading Component: %s\n", name)
	}

	buf := new(bytes.Buffer)
	if _, err := buf.ReadFrom(templ); err != nil {
		return err
	}

	t.RLock()
	_, ok := t.components[name]
	t.RUnlock()

	if ok {
		return ErrTemplateExists
	}

	t.Lock()
	t.components[name] = buf.String()
	t.Unlock()

	if buf.Len() > t.size {
		t.SetBufPoolSize(buf.Len())
	}

	return nil
}

// listTemplates will list all of the names of the templates
func (t *templates) listTemplates(cache templateCache) []string {
	var templs []string

	t.RLock()
	defer t.RUnlock()

	for k := range cache {
		templs = append(templs, k)
	}

	return templs
}

// ListLayouts will list the names of all Layout templates
func (t *templates) ListLayouts() []string {
	return t.listTemplates(t.layouts)
}

// ListComponents will list the names of all Component templates
func (t *templates) ListComponents() []string {
	var templs []string

	t.RLock()
	defer t.RUnlock()

	for k := range t.components {
		templs = append(templs, k)
	}

	return templs
}

// ListPages will list the names of all Content templates
func (t *templates) ListPages() []string {
	return t.listTemplates(t.pages)
}

// ListFuncs will list the names of all Funcs in the FuncMap
func (t *templates) ListFuncs() []string {
	var funcs []string

	t.RLock()
	defer t.RUnlock()

	for k := range t.funcs {
		funcs = append(funcs, k)
	}

	return funcs
}

// Compile will create a template based on the given layout and page
func (t *templates) Compile(layout string, page string) (*template.Template, error) {
	layout = fixName(layout)
	page = fixName(page)

	var templs []string

	t.RLock()
	layoutTempl, ok := t.layouts[layout]
	t.RUnlock()

	if !ok {
		fmt.Printf("Layout ERROR: %s\n", layout)
		return nil, ErrTemplateNotFound
	}

	t.RLock()
	pageTempl, ok := t.pages[page]
	t.RUnlock()

	if !ok {
		fmt.Printf("Page ERROR: %s\n", page)
		return nil, ErrTemplateNotFound
	}

	fmt.Printf("Page: %s\nComponents: %v\nLayout: %s\nComponents: %v\n", pageTempl.name, pageTempl.components, layoutTempl.name, layoutTempl.components)

	templs = append(templs, layoutTempl.template, pageTempl.template)

	for _, component := range uniqueStringSlice(append(layoutTempl.components, pageTempl.components...)) {
		component = fixName(component)

		var componentTempl string

		t.RLock()
		componentTempl, ok = t.components[component]
		t.RUnlock()

		if !ok {
			fmt.Printf("component ERROR: %s\n", component)
			return nil, ErrTemplateNotFound
		}

		templs = append(templs, componentTempl)
	}

	stringTemplate := strings.Join(templs, "\n\n")
	compiledTemplate := template.Must(template.New("").Funcs(t.funcs).Parse(stringTemplate))

	if t.OutputToDisk {
		filename := strings.Replace(page, string(os.PathSeparator), ".", -1)
		filePath := fixName(fmt.Sprintf("%s/%s", t.OutputPath, filename))

		var dirErr error

		t.Lock()
		defer t.Unlock()
		if _, err := os.Stat(t.OutputPath); os.IsNotExist(err) {
			dirErr = os.MkdirAll(t.OutputPath, os.ModePerm)
		}

		if dirErr == nil {
			fmt.Printf("Saving template to: %s\n", filePath)
			err := ioutil.WriteFile(filePath, []byte(stringTemplate), os.ModePerm)
			if err != nil {
				fmt.Printf("Error writing file: %s\n", err)
			}
		} else {
			return nil, dirErr
		}
	}

	fmt.Printf("Compiled Page: %s\n", page)

	return compiledTemplate, nil
}

// RegisterAndCompile will register given templates if not already and then will compile the template
func (t *templates) RegisterAndCompile(layout string, page string, components ...string) (*template.Template, error) {
	if _, err := t.NewPageFromFile(page, components...); err != nil {
		return nil, err
	}

	return t.Compile(layout, page)
}

// Render writes a template to the response writer.
// Use this for dynamic pages that change during every view
func (t *templates) Render(w http.ResponseWriter, templ *template.Template, data interface{}) (int64, error) {
	// Get a buffer
	buf := t.pool.Get()
	defer t.pool.Put(buf)

	// Render template to buffer, return server error if it fails
	if err := templ.Execute(buf, data); err != nil {
		return 0, err
	}

	// Set the content type to HTML and send the template to the browser
	w.Header().Set("Content-Type", "text/html; charset=utf-8")

	return buf.WriteTo(w)
}

// RenderToBytes will render the template to a byte slice.
// This is useful for static pages that can be rendered during startup
func (t *templates) RenderToBytes(templ *template.Template, data interface{}) ([]byte, error) {
	// Get a buffer
	buf := t.pool.Get()
	defer t.pool.Put(buf)

	// Render template to buffer, return server error if it fails
	if err := templ.Execute(buf, data); err != nil {
		return nil, err
	}

	return buf.Bytes(), nil
}
