package gohtml

import (
	"html/template"
	"strings"
	"testing"
)

var outputPath = fixName("testAssets/output")

func init() {
	// Cache.Debug = true
	Cache.OutputPath = outputPath
	Cache.OutputToDisk = true
	Cache.Init()
}

// func Test_templates_Compile(t *testing.T) {
// 	type args struct {
// 		layout     string
// 		content    string
// 		components []string
// 	}
// 	tests := []struct {
// 		name    string
// 		t       *templates
// 		args    args
// 		want    *template.Template
// 		wantErr bool
// 	}{
// 		{"CompileIndex", &Cache, args{"testAssets/layouts/main.gohtml", "testAssets/pages/index.gohtml", []string{"testAssets/components/footer.gohtml", "testAssets/components/meta.gohtml", "testAssets/components/navbar.gohtml", "testAssets/components/scripts.gohtml"}}, nil, false},
// 		{"CompileADJA", &Cache, args{"testAssets/layouts/main.gohtml", "testAssets/pages/adja.gohtml", []string{"testAssets/components/footer.gohtml", "testAssets/components/meta.gohtml", "testAssets/components/navbar.gohtml", "testAssets/components/scripts.gohtml"}}, nil, false},
// 		{"CompileGallery", &Cache, args{"testAssets/layouts/main.gohtml", "testAssets/pages/gallery.gohtml", []string{"testAssets/components/footer.gohtml", "testAssets/components/meta.gohtml", "testAssets/components/navbar.gohtml", "testAssets/components/scripts.gohtml"}}, nil, false},
// 		{"CompilePhotobooth", &Cache, args{"testAssets/layouts/main.gohtml", "testAssets/pages/photobooth.gohtml", []string{"testAssets/components/footer.gohtml", "testAssets/components/meta.gohtml", "testAssets/components/navbar.gohtml", "testAssets/components/scripts.gohtml"}}, nil, false},
// 	}
// 	t.Logf("Layouts: %v", Cache.ListLayouts())
// 	t.Logf("Contents: %v", Cache.ListPages())
// 	t.Logf("Components: %v", Cache.ListComponents())
// 	for _, tt := range tests {
// 		t.Run(tt.name, func(t *testing.T) {
// 			got, err := tt.t.Compile(tt.args.layout, tt.args.content, tt.args.components...)
// 			if (err != nil) != tt.wantErr {
// 				t.Errorf("templates.compile() error = %v, wantErr %v", err, tt.wantErr)
// 				return
// 			}
// 			_ = got
// 			// if !reflect.DeepEqual(got, tt.want) {
// 			// 	t.Errorf("templates.compile() = %v, want %v", got, tt.want)
// 			// }
// 		})
// 	}
// }

// Todo: Create Render test
// func Test_templates_Render(t *testing.T) {
// 	type args struct {
// 		w     http.ResponseWriter
// 		templ *template.Template
// 		data  interface{}
// 	}
// 	tests := []struct {
// 		name    string
// 		t       *templates
// 		args    args
// 		want    int64
// 		wantErr bool
// 	}{
// 		// TODO: Add test cases.
// 	}
// 	for _, tt := range tests {
// 		t.Run(tt.name, func(t *testing.T) {
// 			got, err := tt.t.Render(tt.args.w, tt.args.templ, tt.args.data)
// 			if (err != nil) != tt.wantErr {
// 				t.Errorf("templates.Render() error = %v, wantErr %v", err, tt.wantErr)
// 				return
// 			}
// 			if got != tt.want {
// 				t.Errorf("templates.Render() = %v, want %v", got, tt.want)
// 			}
// 		})
// 	}
// }
// Todo: Create RenderToBytes test
// func Test_templates_RenderToBytes(t *testing.T) {
// 	type args struct {
// 		templ *template.Template
// 		data  interface{}
// 	}
// 	tests := []struct {
// 		name    string
// 		t       *templates
// 		args    args
// 		want    []byte
// 		wantErr bool
// 	}{
// 		// TODO: Add test cases.
// 	}
// 	for _, tt := range tests {
// 		t.Run(tt.name, func(t *testing.T) {
// 			got, err := tt.t.RenderToBytes(tt.args.templ, tt.args.data)
// 			if (err != nil) != tt.wantErr {
// 				t.Errorf("templates.RenderToBytes() error = %v, wantErr %v", err, tt.wantErr)
// 				return
// 			}
// 			if !reflect.DeepEqual(got, tt.want) {
// 				t.Errorf("templates.RenderToBytes() = %v, want %v", got, tt.want)
// 			}
// 		})
// 	}
// }

func Test_templates_StandardOperation(t *testing.T) {
	t.Run("Load", func(t *testing.T) {
		TestGlobalNewLayoutFromFile(t)
	})

	t.Log("Layouts:")
	for _, templ := range Cache.ListLayouts() {
		t.Log(templ)
	}

	t.Run("Compile", func(t *testing.T) {
		TestGlobalAddFuncs(t)
		TestGlobalRegisterAndCompile(t)
	})

	t.Log("Layouts:")
	for _, templ := range Cache.ListLayouts() {
		t.Log(templ)
	}

	t.Log("Funcs:")
	for _, templ := range Cache.ListFuncs() {
		t.Log(templ)
	}

	t.Log("Pages:")
	for _, templ := range Cache.ListPages() {
		t.Log(templ)
	}

	t.Log("Components:")
	for _, templ := range Cache.ListComponents() {
		t.Log(templ)
	}
}

func TestGlobalNewLayoutFromFile(t *testing.T) {
	type args struct {
		filePath   string
		components []string
	}
	components := []string{
		"testAssets/templates/components/L1.gohtml",
		"testAssets/templates/components/L2.gohtml",
		"testAssets/templates/components/L3.gohtml",
		"testAssets/templates/components/L4.gohtml",
		"testAssets/templates/components/L5.gohtml",
	}
	tests := []struct {
		name    string
		templ   *templates
		args    args
		wantErr bool
	}{
		{"NewLayout-1", &Cache, args{"testAssets/templates/layouts/1.gohtml", components}, false},
		{"NewLayout-2", &Cache, args{"testAssets/templates/layouts/2.gohtml", components}, false},
		{"NewLayout-3", &Cache, args{"testAssets/templates/layouts/3.gohtml", nil}, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt := tt
			t.Parallel()
			_, err := tt.templ.NewLayoutFromFile(tt.args.filePath, tt.args.components...)
			if (err != nil && err != ErrTemplateExists) != tt.wantErr {
				t.Errorf("templates.NewLayoutFromFile() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
		})
	}
}

func TestGlobalAddFuncs(t *testing.T) {
	type args struct {
		functions template.FuncMap
	}
	tests := []struct {
		name    string
		templ   *templates
		args    args
		wantErr bool
	}{
		{"AddFuncs-Strings", &Cache, args{template.FuncMap{"Title": strings.Title, "ToLower": strings.ToLower, "ToUpper": strings.ToUpper}}, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := tt.templ.AddFuncs(tt.args.functions); (err != nil && err != ErrFuncExists) != tt.wantErr {
				t.Errorf("templates.AddFuncs() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestGlobalRegisterAndCompile(t *testing.T) {
	type args struct {
		layout     string
		page       string
		components []string
	}
	components := []string{
		"testAssets/templates/components/I1.gohtml",
		"testAssets/templates/components/I2.gohtml",
		"testAssets/templates/components/I3.gohtml",
		"testAssets/templates/components/I4.gohtml",
		"testAssets/templates/components/I5.gohtml",
	}
	tests := []struct {
		name    string
		templ   *templates
		args    args
		wantErr bool
	}{
		{"NewPage-1.1", &Cache, args{"testAssets/templates/layouts/1.gohtml", "testAssets/templates/pages/1.gohtml", components}, false},
		{"NewPage-1.2", &Cache, args{"testAssets/templates/layouts/1.gohtml", "testAssets/templates/pages/2.gohtml", components}, false},
		{"NewPage-2.1", &Cache, args{"testAssets/templates/layouts/2.gohtml", "testAssets/templates/pages/3.gohtml", components}, false},
		{"NewPage-2.2", &Cache, args{"testAssets/templates/layouts/2.gohtml", "testAssets/templates/pages/4.gohtml", components}, false},
		{"NewPage-3.1", &Cache, args{"testAssets/templates/layouts/3.gohtml", "testAssets/templates/pages/5.gohtml", components}, false},
		{"NewPage-3.2", &Cache, args{"testAssets/templates/layouts/3.gohtml", "testAssets/templates/pages/6.gohtml", components}, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			// i := i
			tt := tt
			t.Parallel()
			// tt.templ.OutputPath = fmt.Sprintf("%s%c%d", outputPath, os.PathSeparator, i)
			_, err := tt.templ.RegisterAndCompile(tt.args.layout, tt.args.page, tt.args.components...)
			if (err != nil && err != ErrTemplateExists) != tt.wantErr {
				t.Errorf("templates.RegisterAndCompile() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
		})
	}
}

// func TestGlobalRender(t *testing.T) {
// 	type args struct {
// 		w     http.ResponseWriter
// 		templ *template.Template
// 		data  interface{}
// 	}
// 	tests := []struct {
// 		name    string
// 		templ   *templates
// 		args    args
// 		want    int64
// 		wantErr bool
// 	}{
// 		// TODO: Add test cases.
// 	}
// 	for _, tt := range tests {
// 		t.Run(tt.name, func(t *testing.T) {
// 			got, err := tt.templ.Render(tt.args.w, tt.args.templ, tt.args.data)
// 			if (err != nil) != tt.wantErr {
// 				t.Errorf("templates.Render() error = %v, wantErr %v", err, tt.wantErr)
// 				return
// 			}
// 			if got != tt.want {
// 				t.Errorf("templates.Render() = %v, want %v", got, tt.want)
// 			}
// 		})
// 	}
// }

// func TestGlobalRenderToBytes(t *testing.T) {
// 	type args struct {
// 		templ *template.Template
// 		data  interface{}
// 	}
// 	tests := []struct {
// 		name    string
// 		templ   *templates
// 		args    args
// 		want    []byte
// 		wantErr bool
// 	}{
// 		// TODO: Add test cases.
// 	}
// 	for _, tt := range tests {
// 		t.Run(tt.name, func(t *testing.T) {
// 			got, err := tt.templ.RenderToBytes(tt.args.templ, tt.args.data)
// 			if (err != nil) != tt.wantErr {
// 				t.Errorf("templates.RenderToBytes() error = %v, wantErr %v", err, tt.wantErr)
// 				return
// 			}
// 			if !reflect.DeepEqual(got, tt.want) {
// 				t.Errorf("templates.RenderToBytes() = %v, want %v", got, tt.want)
// 			}
// 		})
// 	}
// }
