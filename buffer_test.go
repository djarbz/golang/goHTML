package gohtml

import "testing"

func Test_buffer_SetBufPoolSize(t *testing.T) {
	type args struct {
		size int
	}
	tests := []struct {
		name string
		b    *buffer
		args args
	}{
		{"SetBufPoolSize-64", &Cache.buffer, args{64}},
		{"SetBufPoolSize-128", &Cache.buffer, args{128}},
		{"SetBufPoolSize-256", &Cache.buffer, args{256}},
		{"SetBufPoolSize-512", &Cache.buffer, args{512}},
		{"SetBufPoolSize-1024", &Cache.buffer, args{1024}},
		{"SetBufPoolSize-2048", &Cache.buffer, args{2048}},
		{"SetBufPoolSize-4096", &Cache.buffer, args{4096}},
		{"SetBufPoolSize-8192", &Cache.buffer, args{8192}},
		{"SetBufPoolSize-1024", &Cache.buffer, args{16384}},
		{"SetBufPoolSize-2048", &Cache.buffer, args{32768}},
		{"SetBufPoolSize-4096", &Cache.buffer, args{65536}},
		{"SetBufPoolSize-8192", &Cache.buffer, args{131072}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			t.Parallel()
			tt.b.SetBufPoolSize(tt.args.size)
			got := tt.b.GetBufPoolSize()
			want := tt.b.size
			if got != want {
				t.Errorf("buffer.GetBufPoolSize() = %v, want %v", got, want)
			}
		})
	}
}
