package gohtml

import (
	"sync"

	"github.com/oxtoacart/bpool"
)

// https://blog.questionable.services/article/using-buffer-pools-with-go/
type buffer struct {
	size int
	pool *bpool.BufferPool
	sync.RWMutex
}

// SetBufPoolSize will change the buffer pool to the specified size
//
// Should only be run during startup, prior to rendering
func (b *buffer) SetBufPoolSize(size int) {
	b.Lock()
	defer b.Unlock()
	b.size = size
	b.pool = bpool.NewBufferPool(b.size)
}

// GetBufPoolSize will report the buffer pool size
func (b *buffer) GetBufPoolSize() int {
	b.RLock()
	defer b.RUnlock()
	return b.size
}
