package gohtml

import (
	"os"
	"strings"
)

// fixName will change a given name to match the current filesystem path separator
func fixName(name string) string {
	return strings.Replace(strings.Replace(name, "/", string(os.PathSeparator), -1), "\\", string(os.PathSeparator), -1)
}

// stringInSlice returns true if the slice contains the string
func stringInSlice(a string, list []string) bool {
	for _, b := range list {
		if b == a {
			return true
		}
	}
	return false
}

// uniqueStringSlice will remove duplicate values in a slice
func uniqueStringSlice(slice []string) []string {
	keys := make(map[string]bool)
	var list []string
	for _, entry := range slice {
		if _, value := keys[entry]; !value {
			keys[entry] = true
			list = append(list, entry)
		}
	}
	return list
}
